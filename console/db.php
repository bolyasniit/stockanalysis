<?php
    $serverName = "localhost";
    $user = "homestead";
    $password = "secret";
    $dbname = "homestead";

    try {
        $pdo = new PDO("pgsql:dbname=$dbname;host=$serverName;",$user,$password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $sql = "DROP TABLE IF EXISTS STOCKS; CREATE TABLE STOCKS(id SERIAL PRIMARY KEY, symbol VARCHAR(10),name VARCHAR(100), sector VARCHAR(100), industry VARCHAR(100))";
        $pdo->exec($sql);
        populateDB($pdo, "companylist.csv");
        echo "Script was executed successfully\n";
    } catch (PDOException $e) {
        echo $e->getMessage();
    } finally {
        unset($pdo);
    }

    function populateDB($pdo, $filename) {
        $file = fopen($filename,'r');
        $ps = $pdo->prepare("INSERT INTO STOCKS(symbol, name, sector, industry) VALUES (:symbol,:name,:sector,:industry)");
        fgetcsv($file);
        while (!feof($file)) {
            $line = fgetcsv($file);
            $ps->bindParam(":symbol", $line[0]);
            $ps->bindParam(":name", $line[1]);
            $ps->bindParam(":sector", $line[2]);
            $ps->bindParam(":industry", $line[3]);
            if ($ps->execute()) {
                echo "Query ran successfully\n";
            } else {
                echo "Query failed";
            }
        }
        fclose($file);

    }

    function getEntry($entryName) {
        $ps = $pdo->prepare("SELECT ".$entryName." FROM STOCKS");
        if ($ps->exec()) {
            return $ps->fetch();
        }
    }
?>