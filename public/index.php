<!DOCTYPE HTML>
<html>
    <head>
        <title>Stock Analyst</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Bitter:700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="style.css">

    </head>

    <body>
        <section id="mainContainer">
            <header id="nav">
                <div class="inner">
                    <a href="index.php" style="text-decoration: none"><h3 class="masthead-brand text-white">Stock Analyst</h3></a>
                </div>
            </header>

            <section id="selection">
                <section class="row">
                    <section class="col-md-12" id="skyline" style="width: 5000px;">
                        <h1 class="text-white text-outline" style="margin-top: 40px;font-family: 'Bitter', serif;">Invest Now!</h1>
                        <h3 class=" mr-md text-white text-outline" style="color:#0273e3">Choose the company you want to research: </h3>
                        <form action="" method="GET" id="form">
                            <?php getEntry("symbol") ?>
                            <input class="btn btn-secondary" id="submit" type="submit"/>
                        </form>
                    </section>
					<section class="container" id="footer">
						<section class="row">
							<section class="col-md-12">
								<h3> Created by: </h3>
							</section>
							<br/>
							<section class="col-md-6">
								<span> Itai Bolyasni - 1633867</span>
							</section>
							<section class="col-md-6">
								<span> Shirin</span>
							</section>
						</section>
					</section>
                    <section class="wrapper">
                    <section class="text-left" id="first">
                        <section class="header bg-light text-dark">
                            <h2 class="api_header">Lorem Ipsum</h2>
                        </section>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                    </section>
                    <section class="text-left" id="second">
                        <section class="header bg-secondary">
                            <h2 class="api_header">Lorem Ipsum</h2>
                        </section>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                    </section>
                    </section>
                </section>
            </section>

        </section>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>

<?php
function getEntry($entryName) {
    $serverName = "localhost";
    $user = "homestead";
    $password = "secret";
    $dbname = "homestead";

    try {
        $pdo = new PDO("pgsql:dbname=$dbname;host=$serverName;", $user, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $ps = $pdo->prepare("SELECT * FROM STOCKS");
        //$ps->bindParam(1,$entryName);
        if ($ps->execute()) {
            echo "<select id='options'>";
            while ($row = $ps->fetch()) {
                echo "<option value=$row[$entryName]>$row[$entryName]"." - ".$row['name']."</option>";
            }
            echo "</select>";
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
?>